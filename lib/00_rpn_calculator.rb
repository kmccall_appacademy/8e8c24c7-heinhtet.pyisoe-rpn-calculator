class RPNCalculator

  def initialize
    @stack = []
  end

  def push(number)
    @stack << number
  end

  def value
    @stack.last
  end

  def plus
    raise "calculator is empty" if @stack.empty?
    @stack << @stack.pop + @stack.pop
  end

  def minus
    raise "calculator is empty" if @stack.empty?
    first = @stack.pop
    second = @stack.pop
    @stack << second - first
  end

  def divide
    raise "calculator is empty" if @stack.empty?
    first = @stack.pop.to_f
    second = @stack.pop.to_f
    @stack << second / first
  end

  def times
    raise "calculator is empty" if @stack.empty?
    result = @stack.pop * @stack.pop
    @stack << result
  end

  def tokens(string)
  arr = string.split(" ")
  arr.map {|el| "1234567890".include?(el) ?
    el.to_i : el.to_sym}

  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |el|
      case el
      when Integer then push(el)
      when :+ then plus
      when :- then minus
      when :* then times
      when :/ then divide
      end

    end
    value
  end

end #final
